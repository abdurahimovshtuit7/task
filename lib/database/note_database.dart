import 'package:hive/hive.dart';

import 'dart:async';

import '../models/note_model.dart';

class NoteDatabase {
  String _boxName = "Note";
  // open a box
  Future<Box> noteBox() async {
    var box = await Hive.openBox<Note>(_boxName);
    return box;
  }

  // get full note
  Future<List<Note>> getFullNote() async {
    final box = await noteBox();
    List<Note> notes = box.values.toList().cast<Note>();
    return notes;
  }

  // to add data in box
  Future<void> addToBox(Note note) async {
    final box = await noteBox();
    final modelExists = box.containsKey(note.id);
    if (!modelExists) {
      await box.add(note);
    }
  }

  // to add list in box
  Future<void> addAllToBox(List<Note> noteList) async {
    final box = await noteBox();
    await box.addAll(noteList);
  }

  // delete data from box
  Future<void> deleteFromBox(int index) async {
    final box = await noteBox();
    final list = box.toMap();
    dynamic desiredKey;
    list.forEach((key, value){
        if (value.id == index)
            desiredKey = key;
    });

    await box.delete(desiredKey);
  }

  // delete all data from box
  Future<void> deleteAll() async {
    final box = await noteBox();
    await box.clear();
  }

  // update data
  Future<void> updateNote(int id, Note note) async {
    final box = await noteBox();
    final list = box.toMap();
    dynamic desiredKey;
    list.forEach((key, value){
        if (value.id == id)
            desiredKey = key;
    });
      await box.put(desiredKey, note);
  }
}
