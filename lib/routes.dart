import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task/database/note_database.dart';
import 'screens/bloc/note_bloc.dart';
import 'screens/passcodeScreen/passcode_screen.dart';
import 'screens/tabScreen/tab_screen.dart';

class Routes {
  Routes._();
  //static variables
  static const String tabs = TabScreen.routeName;
  static const String passcode = PassCodeScreen.routeName;

  static final routes = <String, WidgetBuilder>{
    tabs: (BuildContext context) => BlocProvider(
          create: (context) => NoteBloc(NoteDatabase()),
          child: TabScreen(),
        ),
    // passcode: (BuildContext context) => PassCodeScreen(isAuthenticated: ,),
  };
}
