import 'package:flutter/material.dart';
import 'colors.dart';

final ThemeData themeData = ThemeData(
    // primarySwatch: Colors,
    primaryColor: AppColor.primary,
    scaffoldBackgroundColor: Colors.white,
    appBarTheme: AppBarTheme(
      elevation: 0.0,
      backgroundColor: AppColor.primary,
      iconTheme: IconThemeData(
        color: AppColor.white, //change your color here
      ),
    ));


