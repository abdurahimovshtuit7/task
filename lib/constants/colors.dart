import 'package:flutter/material.dart';

class AppColor {
  AppColor._();

  static const Color primary = Color(0xFF0277bd);
  static const Color brandColor = Color(0xFF007EBD);
  static const Color black = Color(0xFF000000);
  static const Color blue = Color(0xFF0000FF);

  static const Color red = Colors.red;
 
  static const Color greyishBrown = Color(0xFF4A4A4A);
  static const Color darkWhite = Color(0xFFF5F5F5);
  static const Color white = Color(0xFFFFFFFF);
  static const Color warmGrey = Color(0xFF979797);
  static const Color descriptionText = Color(0xFF7B7E8E);

}
