import 'package:flutter/material.dart';
import 'package:task/constants/colors.dart';

class Modal extends StatefulWidget {
  final Function onSave;
  final String title;
  final String term;
  final String status;
  final int index;
  final int id;
  const Modal(
      {Key? key,
      required this.onSave,
      this.title = '',
      this.term = '1',
      this.status = 'в прогрессе',
      this.index = 0,
      this.id = 0
    })
      : super(key: key);

  @override
  _ModalState createState() => _ModalState();
}

class _ModalState extends State<Modal> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late final titleInput = TextEditingController(text: widget.title);
  late final term = TextEditingController(text: widget.term);
  late final status = TextEditingController(text: widget.status);
  final alphaNumericText = RegExp(r'[a-zA-Z0-9]');

  var termArray = ['1', '2', '3', '5', '7', '10', '15', '20'];
  var statusArray = ["в прогрессе", "выполнено"];

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Container(
        constraints: BoxConstraints(maxHeight: 320),
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text("С полями название:",style:TextStyle(color: AppColor.descriptionText)),
                        TextFormField(
                          controller: titleInput,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Пожалуйста, введите текст';
                            }
                            if (alphaNumericText.hasMatch(value) == false) {
                              return 'Используйте только буквенно-цифровые символы в примечании';
                            }
                            return null;
                          },
                        )
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Срок (дата): ",style:TextStyle(color: AppColor.descriptionText)),
                        _dropDown(_onChangeTerm, term.text, termArray)
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Статус: ",style:TextStyle(color: AppColor.descriptionText)),
                        _dropDown(_onChangeStatus, status.text, statusArray)
                      ]),
                ),
                Expanded(
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        ElevatedButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text("Cancel")),
                        ElevatedButton(
                            onPressed: () => onSaved(), child: Text("Save"))
                      ]),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void onSaved() {
    if (_formKey.currentState!.validate()) {
      widget.onSave(titleInput.text, term.text, status.text,widget.id);
    }
  }

  Widget _dropDown(dynamic onChange, String value, dynamic array) {
    return DropdownButton<String>(
      value: value,
      icon: const Icon(Icons.arrow_drop_down_sharp),
      iconSize: 24,
      elevation: 16,
      style: const TextStyle(color: AppColor.black),
      underline: Container(
        height: 2,
        color: AppColor.primary,
      ),
      onChanged: (String? newValue) {
        onChange(newValue);
      },
      items: array.map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(value),
        );
      }).toList(),
    );
  }

  _onChangeTerm(String newValue) {
    setState(() {
      term.text = newValue;
    });
  }

  _onChangeStatus(String newValue) {
    setState(() {
      status.text = newValue;
    });
  }
}
