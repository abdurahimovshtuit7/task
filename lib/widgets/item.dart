import 'package:flutter/material.dart';
import 'package:task/constants/colors.dart';

class Item extends StatelessWidget {
  final String title;
  final String status;
  final int term;
  final dynamic delete;
  final dynamic edit;
  const Item({ Key? key,required this.title,required this.status,required this.term,required this.delete,required this.edit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
                  // color: AppColor.white,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: AppColor.white,
                      boxShadow: [
                        BoxShadow(
                          color: AppColor.primary.withOpacity(0.5),
                          blurRadius: 4.0,
                        ),
                      ]),
                  margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                           title,
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Row(children: [
                            Text(term.toString()),
                            IconButton(
                                onPressed: delete, icon: Icon(Icons.delete,color: AppColor.red,))
                          ]),
                          Row(children: [
                            Text(status),
                            IconButton(
                                onPressed: edit, icon: Icon(Icons.create,color: AppColor.primary))
                          ]),
                        ],
                      )
                    ],
                  ),
                );
  }
}