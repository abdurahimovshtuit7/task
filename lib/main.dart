import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive/hive.dart';
import 'package:local_auth/local_auth.dart';
import 'package:task/routes.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:task/screens/passcodeScreen/passcode_screen.dart';
import 'package:task/screens/tabScreen/tab_screen.dart';
import 'dart:async';

import 'constants/app_theme.dart';
import 'database/note_database.dart';
import 'models/note_model.dart';
import 'screens/bloc/note_bloc.dart';
import 'utils/local_auth_api.dart';
import 'utils/navigation_services.dart';

void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter<Note>(NoteAdapter());
  await Hive.openBox<Note>("Note");
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  bool isNewUser = true;
  bool isAuthenticated = false;
  bool  isFaceIdAuthenticated = false;
  // final authService = AuthenticationService();

  @override
  void initState() {
    super.initState();
    getUserStatus();
  }

  Future<void> getUserStatus() async {
     final isAvailable = await LocalAuthApi.hasBiometrics();
     final biometrics = await LocalAuthApi.getBiometrics();
     final hasFingerprint = biometrics.contains(BiometricType.fingerprint);
    //  final hasFaceID =  biometrics.contains(BiometricType.face);
     isAuthenticated = await LocalAuthApi.authenticate();
    //  isFaceIdAuthenticated = await LocalAuthApi.authenticateFaceId();
     setState(() {
       isAuthenticated = isAuthenticated;
      //  isFaceIdAuthenticated = isFaceIdAuthenticated;
     });
    
    print('$isAvailable $hasFingerprint  $isAuthenticated' );
  }
  @override
  Widget build(BuildContext context) {
  
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: themeData,
      home:isAuthenticated ? BlocProvider(
          create: (context) => NoteBloc(NoteDatabase()),
          child: TabScreen(),
        ):PassCodeScreen(),
      // initialRoute:initialRoute,
      routes: Routes.routes,
      navigatorKey: NavigationService.instance.navigationKey
    );
  }
}
