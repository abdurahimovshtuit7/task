import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task/constants/colors.dart';
import 'package:task/database/note_database.dart';
import 'package:task/screens/bloc/note_bloc.dart';
import 'package:task/screens/bloc/note_event.dart';
import 'package:task/screens/mainScreen/main_screen.dart';
import 'package:task/screens/producedList/produced_list.dart';
import 'package:task/screens/progressList/progress_list.dart';
import 'package:task/widgets/modal.dart';

class TabScreen extends StatefulWidget {
  const TabScreen({Key? key}) : super(key: key);
  static const String routeName = 'tabs';

  @override
  _TabScreenState createState() => _TabScreenState();
}

class _TabScreenState extends State<TabScreen> {
  int _selectedPageIndex = 0;
  String dropdownValue = "1";
  late NoteBloc _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<NoteBloc>(context);
    super.initState();
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    print("Tabbar");
    return Scaffold(
      appBar: AppBar(
        title: Text("Задачи"),
        actions: [
          _dropDownButton(),
          IconButton(
              onPressed: () {
                _showSimpleModalDialog(context);
              },
              icon: Icon(Icons.add))
        ],
      ),
      body: IndexedStack(
        index: _selectedPageIndex,
        children: const [
          MainScreen(),
          ProgressList(),
          ProducedList(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        backgroundColor: AppColor.white,
        unselectedItemColor: AppColor.warmGrey,
        selectedItemColor: AppColor.brandColor,
        currentIndex: _selectedPageIndex,
        type: BottomNavigationBarType.fixed,
        items: [
          _NavigationBarItem(0, 'Все', Icons.list),
          _NavigationBarItem(1, 'В прогрессе', Icons.low_priority_outlined),
          _NavigationBarItem(2, 'Выполнено', Icons.checklist) //grading_sharp
        ],
      ),
    );
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  // ignore: non_constant_identifier_names
  dynamic _NavigationBarItem(int index, String label, dynamic icon) {
    return BottomNavigationBarItem(
      backgroundColor: AppColor.white,
      icon: Padding(
        padding: const EdgeInsets.all(2.0),
        child: Icon(icon),
      ),
      label: label,
    );
  }

  Widget _dropDownButton() {
    return PopupMenuButton(
        icon: const Icon(
          Icons.filter_list,
          color: AppColor.white,
        ),
        iconSize: 25,
        itemBuilder: (BuildContext context) => <PopupMenuEntry>[
              PopupMenuItem(
                onTap: () {
                  // _sortByTerm(1);
                  _bloc.add(NoteSortByTerm(id: 1));
                },
                child: Text('В порядке возрастания сроков'),
              ),
              PopupMenuItem(
                onTap: () {
                  // _sortByTerm(2);
                  _bloc.add(NoteSortByTerm(id: 2));
                },
                child: Text('В порядке убывания сроков'),
              ),
            ]);
  }

  _showSimpleModalDialog(context) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Modal(onSave: _onSave,);
        });
  }

  void _onSave(String title, String term, String status, int index) {
    int termParseInt = int.parse(term);
    String date =  DateTime.now().millisecondsSinceEpoch.toString();
    int id = int.parse(date.substring((date.length - 8).clamp(0, date.length)));

    _bloc.add(NoteAddEvent(title: title, status: status, term: termParseInt ,id:id));
    Navigator.pop(context);
  }
}
