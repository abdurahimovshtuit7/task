
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class NoteEvent extends Equatable {
  @override
  List<Object> get props => [];
}

// initial event
class NoteInitialEvent extends NoteEvent {

}
// add event
class NoteAddEvent extends NoteEvent {
  final String title, status;
  final int term,id;

  NoteAddEvent({required this.title, required this.status,required this.term,required this.id});
}
// edit event
class NoteEditEvent extends NoteEvent {
  final String title, status;
  final int term;
  final int id;

  NoteEditEvent(
      {required this.title, required this.status,required this.term,required this.id});
}
// delete event
class NoteDeleteEvent extends NoteEvent {
  final int index;

  NoteDeleteEvent({required this.index});
}

class NoteSortByTerm extends NoteEvent {
 final int id;
  NoteSortByTerm({required this.id});

}