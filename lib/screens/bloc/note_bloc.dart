import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task/database/note_database.dart';
import 'package:task/models/note_model.dart';

import 'note_event.dart';
import 'note_state.dart';

class NoteBloc extends Bloc<NoteEvent, NoteState> {
  final NoteDatabase _noteDatabase;
  List<Note> _notes = [];
  NoteBloc(this._noteDatabase) : super(NoteInitial());
  @override
  Stream<NoteState> mapEventToState(NoteEvent event) async* {
    if (event is NoteInitialEvent) {
      yield* _mapInitialEventToState();
    }

    if (event is NoteAddEvent) {
      yield* _mapNoteAddEventToState(
          title: event.title, status: event.status, term: event.term,id: event.id);
    }

    if (event is NoteEditEvent) {
      yield* _mapNoteEditEventToState(
          title: event.title,
          status: event.status,
          term: event.term,
          id: event.id);
    }
    if (event is NoteSortByTerm) {
      yield* _mapNoteSortByTermEventToState(id: event.id);
    }

    if (event is NoteDeleteEvent) {
      yield* _mapNoteDeleteEventToState(index: event.index);
    }
  }

  // Stream Functions
  Stream<NoteState> _mapInitialEventToState() async* {
    yield NotesLoading();
    await _getNotes();
    yield YourNotesState(notes: _notes);
  }

  Stream<NoteState> _mapNoteAddEventToState(
      {required String title,
      required String status,
      required int term,
      required int id}) async* {
    yield NotesLoading();
    await _addToNotes(title: title, status: status, term: term,id: id);

    yield YourNotesState(notes: _notes);
  }

  Stream<NoteState> _mapNoteEditEventToState(
      {required String title,
      required String status,
      required term,
      required int id}) async* {
    yield NotesLoading();
    await _updateNote(
        newTitle: title, newStatus: status, newTerm: term,id: id);
    yield YourNotesState(notes: _notes);
  }

  Stream<NoteState> _mapNoteSortByTermEventToState({required int id}) async* {
    yield NotesLoading();
    await _getNotes();

    if (id == 1) {
      _notes.sort((a, b) {
        return a.term.compareTo(b.term);
      });
    }
    if (id == 2) {
      _notes.sort((b, a) {
        return a.term.compareTo(b.term);
      });
    }
    await _noteDatabase.deleteAll();
    await _noteDatabase.addAllToBox(_notes);
    await _getNotes();

    yield YourNotesState(notes: _notes);
  }

  Stream<NoteState> _mapNoteDeleteEventToState({required int index}) async* {
    yield NotesLoading();
    await _removeFromNotes(index: index);
    _notes.sort((a, b) {
      var aDate = a.title;
      var bDate = b.title;
      return aDate.compareTo(bDate);
    });
    yield YourNotesState(notes: _notes);
  }

  // Helper Functions
  Future<void> _getNotes() async {
    await _noteDatabase.getFullNote().then((value) {
      _notes = value;
    });
  }

  Future<void> _addToNotes(
      {required String title,
      required String status,
      required int term,
      required int id}) async {
    await _noteDatabase
        .addToBox(Note(title: title, status: status, term: term,id: id));
    await _getNotes();
  }

  Future<void> _updateNote(
      { required String newTitle,
      required String newStatus,
      required int newTerm,
      required int id}) async {
    await _noteDatabase.updateNote(
        id, Note(title: newTitle, status: newStatus, term: newTerm,id: id));
    await _getNotes();
  }

  Future<void> _removeFromNotes({required int index}) async {
    await _noteDatabase.deleteFromBox(index);
    await _getNotes();
  }
}
