import 'dart:async';

import 'package:flutter/material.dart';
import 'package:passcode_screen/circle.dart';
import 'package:task/constants/colors.dart';
import 'package:passcode_screen/keyboard.dart';
import 'package:passcode_screen/passcode_screen.dart';
import 'package:task/utils/navigation_services.dart';
import 'package:task/utils/shared_pref.dart';

import '../../routes.dart';

class PassCodeScreen extends StatefulWidget {
  // final bool isAuthenticated;
  const PassCodeScreen({
    Key? key,
  }) : super(key: key);
  static const routeName = 'passcode-screen';

  @override
  _PassCodeScreenState createState() => _PassCodeScreenState();
}

// const storedPasscode = '1234';

class _PassCodeScreenState extends State<PassCodeScreen> {
  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();
  bool isAuthenticated = false;
  late final String storedPasscode;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() async {
    if (!isAuthenticated) {
      NavigationService.instance.goback();
    }
    _verificationNotifier.close();

    super.dispose();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
 
    await Future.delayed(Duration.zero);
    _showLockScreen(
      context,
      opaque: false,
      cancelButton: Text(
        'Cancel',
        style: const TextStyle(fontSize: 16, color: Colors.white),
        semanticsLabel: 'Cancel',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Container());
  }

  _showLockScreen(
    BuildContext context, {
    required bool opaque,
    CircleUIConfig? circleUIConfig,
    KeyboardUIConfig? keyboardUIConfig,
    required Widget cancelButton,
    List<String>? digits,
  }) {
    Navigator.push(
      context,
      PageRouteBuilder(
        opaque: opaque,
        pageBuilder: (context, animation, secondaryAnimation) => PasscodeScreen(
          title: Text(
            'Enter App ToDo',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 28),
          ),
          circleUIConfig: circleUIConfig,
          keyboardUIConfig: keyboardUIConfig,
          passwordEnteredCallback: _onPasscodeEntered,
          cancelButton: cancelButton,
          deleteButton: Text(
            'Delete',
            style: const TextStyle(fontSize: 16, color: Colors.white),
            semanticsLabel: 'Delete',
          ),
          shouldTriggerVerification: _verificationNotifier.stream,
          backgroundColor: Colors.black.withOpacity(0.8),
          // cancelCallback: _onPasscodeCancelled,
          digits: digits,
          passwordDigits: 4,
          // bottomWidget: _buildPasscodeRestoreButton(),
        ),
      ),
    );
  }

  _onPasscodeEntered(String enteredPasscode) async {
     var storedPasscode = await SharedPref().read('code') ?? '';
    bool isValid=false;
    if (storedPasscode.isNotEmpty) {
       isValid = storedPasscode == enteredPasscode;
      _verificationNotifier.add(isValid);
      
    } else if (storedPasscode.isEmpty) {
      await SharedPref().save('code', enteredPasscode);
       isValid =true;
      _verificationNotifier.add(isValid);
    }
    if (isValid) {
        setState(() {
          isAuthenticated = isValid;
        });
        Navigator.of(context)
            .pushNamedAndRemoveUntil(Routes.tabs, ModalRoute.withName('tabs'));
        Navigator.maybePop(context);
      }
  }
}
