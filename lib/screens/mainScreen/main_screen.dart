import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task/constants/colors.dart';
import 'package:task/screens/bloc/note_bloc.dart';
import 'package:task/screens/bloc/note_event.dart';
import 'package:task/screens/bloc/note_state.dart';
import 'package:task/widgets/item.dart';
import 'package:task/widgets/modal.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  late NoteBloc _bloc;

  @override
  void initState() {
    _bloc = BlocProvider.of<NoteBloc>(context);
    _bloc.add(NoteInitialEvent());

    super.initState();
  }

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Container(child: BlocBuilder<NoteBloc, NoteState>(
        builder: (context, state) {
          if (state is NotesLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is YourNotesState) {
            if (state.notes.isNotEmpty) {
              var list = state.notes;
              return ListView.builder(
                  controller: PrimaryScrollController.of(context),
                  itemCount: state.notes.length,
                  itemBuilder: (context, index) {
                    var item = list[index];
                    return Item(
                      title: item.title,
                      status: item.status,
                      term: item.term,
                      delete: () => _delete(item.id),
                      edit: () => _editNote(context, item.title,
                          item.term.toString(), item.status, index,item.id),
                    );
                  });
            }
          }
          return Container();
        },
      )),
    );
  }

  _delete(int index) {
    print(index);
    _bloc.add(NoteDeleteEvent(index: index));
  }

  _editNote(context, String title, String term, String status, int index,int id) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Modal(
              onSave: _onEdit,
              title: title,
              term: term,
              status: status,
              index: index,
              id:id);
        });
  }

  void _onEdit(String title, String term, String status,int id) {
    int termParseInt = int.parse(term);
    _bloc.add(NoteEditEvent(
        title: title, status: status, term: termParseInt,id: id));
    Navigator.pop(context);
  }
}
