import 'package:hive/hive.dart';
part 'note_model.g.dart';


class Note {
  @HiveType(typeId: 1)
  
  @HiveField(0)
  final String title;
  @HiveField(1)
  final String status;
  @HiveField(2)
  final int term;
  @HiveField(3)
   int id;

  Note({required this.title, required this.status,required this.term,required this.id});

}